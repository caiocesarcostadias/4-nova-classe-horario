package horario;

public class Horario {

	private int segundos;

	public Horario() {
		segundos = 0;
	}

	public Horario(int hora, int minuto, int segundo) {
		setSegundo(segundo);
		setMinuto(minuto);
		setHora(hora);
	}

	public int getHora() {
		return converterTempo()[0];
	}

	public void setHora(int hora) {
		if (!(hora >= 0 && hora < 24)) {
			throw new IllegalArgumentException("Hora inválida!");
		}
		segundos = hora * 3600 + (segundos - (getHora() * 3600));
	}

	public int getMinuto() {
		return converterTempo()[1];
	}

	public void setMinuto(int minuto) {
		if (!(minuto >= 0 && minuto < 60)) {
			throw new IllegalArgumentException("Minuto inválido!");
		}
		segundos = minuto * 60 + (segundos - (getMinuto() * 60));
	}

	public int getSegundo() {
		return converterTempo()[2];
	}

	public void setSegundo(int segundo) {
		if (!(segundo >= 0 && segundo < 60)) {
			throw new IllegalArgumentException("Segundo inválido!");
		}
		segundos = segundo + (segundos - getSegundo());
	}

	public void incrementaHora() {
		if (getHora() == 23)
			segundos -= 3600 * 23;
		else
			segundos += 3600;
	}

	public void incrementaMinuto() {
		if (getMinuto() == 59) {
			segundos -= 60 * 59;
			incrementaHora();
		} else
			segundos += 60;
	}

	public void incrementaSegundo() {
		if (ehUltimoSegundo()) {
			segundos = 0;
		} else
			segundos++;
	}
	
	private boolean ehUltimoSegundo() {
		return segundos == 86399;
	}

	private int[] converterTempo() {
		int hora = segundos / 3600;
		int minuto = (segundos - 3600 * hora) / 60;
		int segundo = segundos - (3600 * hora) - (60 * minuto);

		int[] horario = { hora, minuto, segundo };

		return horario;
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", getHora(), getMinuto(), getSegundo());
	}
}
